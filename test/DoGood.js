var events = require('./../app/javascripts/events');
var DoGood = artifacts.require('DoGood.sol');
var Company = artifacts.require('Company.sol');

//************************************************
// Tests
contract('DoGood', function(accounts) {
  const account1 = accounts[0];
  const account2 = accounts[1];
  const account3 = accounts[2];

  var doGood;

//     // TEST: Try to send ether to DoGood- should fail

  // TEST: doGood.registerCompany
  // TEST: Event_CompanyCreated
  it("can create a new company", function() {
    var companyName = "Test";
    var totalSharesOutstanding = 1000;
    var totalFloat = 100;
    const adminAddress = '0x55928c1d15ae0c55ad452ec870f035bc1012cf51';

    return DoGood.deployed().then(function(instance) {
      doGood = instance;

      return doGood.registerCompany(companyName, adminAddress, totalSharesOutstanding, totalFloat);
    }).then(function (result) {
      companyAddress = events.CompanyCreated(result);

      return Company.at(companyAddress);
    });
  });

  it("can delete a company", function() {
    // TODO
  });

  it("can change admin", function() {
    // TODO
  });

  it('looks up the company address, given name, correctly', function () { 
    ;    // TODO:
  });

});

