const sha3 = require('solidity-sha3')

var events = require('./../app/javascripts/events');
var DoGood = artifacts.require('DoGood.sol');
var Company = artifacts.require('Company.sol');

const DEBUG_PRINT_RUNNING_BALANCES = true; // NOTE: This can be incorrect if printing in middle of failed promise

//************************************************
// Tests
contract('Company', function (accounts) {

  const account1 = accounts[0];
  const account2 = accounts[1];
  const account3 = accounts[2];
  const account4 = accounts[3];

  var doGood;
  var company;
  var companyAddress;

  const companyName = "Acme Bricks";
  const totalSharesOutstanding = 1000;
  const totalFloat = 200;
  const companyAdmin = account1;
  const doGoodAdmin = '0x55928c1d15ae0c55ad452ec870f035bc1012cf51';

  var balanceSheet;

  // Print out account numbers
  var _ = 0;
  console.log('Accounts');
  console.log(accounts.map(function(s){_+=1; return 'Account ' + _+': ' + s}));

  // function balanceOf(address who) returns (uint)
  it('creates a new company that is correctly initialized', function () {

    return DoGood.deployed().then(function(instance) {
      doGood = instance;

      return doGood.registerCompany( companyName
                                   , companyAdmin
                                   , totalSharesOutstanding
                                   , totalFloat, {from: account1});
    }).then(function (result) {
      companyAddress = events.CompanyCreated(result);

      return Company.at(companyAddress);
    }).then(function (instance) {
      company = instance;
      balanceSheet = runningBalance(company);

      return balanceSheet([ [companyAddress, totalSharesOutstanding]
                          , [account1 , 0]
                          ], '1');
    });
  });

  // event Approval(address indexed owner, address indexed spender, uint value);
  // function approve(address spender, uint value) returns (bool ok)
  // function allowance(address owner, address spender) returns (uint)   TODO:
  // function transfer(address to, uint256 value) returns (bool success)
  // function transferFrom(address from, address to, uint value) returns (bool ok)
  it('sends and approves balances correctly', function () {
    const amount1 = 15;
    const allowance = 10;
    const spentAmount = 5;

    return company.sendCompanyShares(account3, amount1, {companyAdmin}).then(function(){

      return balanceSheet([ [account3, amount1]
                          , [account4, 0]
                          , [companyAddress, -amount1]
                          ], '2');
    }).then(function() {

      return company.approve(account4, allowance, {from: account3});
    }).then(function(result){
      [owner, spender, value] = events.Approval(result);
      assert(owner == account3, 'Owner address is incorrect. \nExp ' + account3 + '\nAct ' + owner);
      assert(spender == account4, 'Spender address is incorrect');
      assert(value == allowance, 'Value is incorrect');

      return company.allowance(account3, account4);
    }).then(function(actualAllowance) {
      assert(allowance == actualAllowance, 'Expected allowance is wrong. Exp ' + allowance + '; Act ' + actualAllowance);

      return company.transferFrom(account3, account4, spentAmount, {from: account4});
    }).then(function(tx){

      return balanceSheet([ [account3, -spentAmount]
                          , [account4, spentAmount]
                          ], '3');
    }).then(function() {

      // Transfer too much
      return company.transferFrom(account3, account4, allowance, {from: account4}).catch(function(e){console.log('This error should be here!')});
    }).then(function(tx){
      
      // No changes
      return balanceSheet([ [account3, 0]
                          , [account4, 0]
                          , [companyAddress, 0]
                          ], '4');
    });
  });

    //     // TEST: Try to send ether to company- should fail
    //     // TEST: Make sure company can not errenously give to child token (via transfer/transferFrom)



 
  // function redeemShares(address receiver, bytes32 redeemCode) {
  // function createNewRedeemableShares(uint value, uint expireTimeSecondsInFuture, bytes32 code)
  it('redeems shares correctly', function() {
    const secretCode = '0x0ef9d8f8804d174666011a394cab7901679a8944d24249fd148a6a36071151f8';
    // const secretCode = 'DoGood FTW!!';
    const nonsecretCode = sha3.default(secretCode);
    const redeemValue = 10;
    

      // Expire test. Balances should be same afterwards
    return company.createNewRedeemableShares(redeemValue, 0, nonsecretCode,{from: companyAdmin}).then(function(result) {
      
      return company.redeemShares(account2, secretCode);
    }).then(function(result){

      return balanceSheet([ [account2, 0]
                          , [companyAddress, 0]
                          ], '5');      
    }).then(function(result){

      return company.createNewRedeemableShares(redeemValue, 200, nonsecretCode,{from: companyAdmin});
    }).then(function(result){

      return company.redeemShares(account2, secretCode);
    }).then(function(result){
      
      return balanceSheet([ [account2, redeemValue]
                          , [companyAddress, -redeemValue]
                          ], '6');     
    });
  });
  
  it('changes company admin', function() {
    ;
  });

  it('runs cleanup correctly', function() {
    ;
  });


});


//************************************************
// Private functions

var runningBalance = function(company_) {
  // [address: balance, ...]
  var balances = {};
  var company = company_;

  // Checks balances of addresses match expected
  // arr- [(addr1, deduction), (a2, d2), ..]
  // Returns an array of promises
  return function(arr, msg) {

    // Add/deduct balances
    arr.map(function(x){
      var addr = x[0];
      if (balances[addr] == undefined)
        balances[addr] = 0;
      balances[addr] = balances[addr] + x[1];
    });

    var f = function (t) {
      var addr, expectedBalance
      [addr, expectedBalance] = t;
      return company.balanceOf(addr).then(function(actualBalance) {
        if (DEBUG_PRINT_RUNNING_BALANCES)
          console.log(addr + ': ' + actualBalance);
        assert( actualBalance == expectedBalance 
              , msg + '\n' + 'Unexpected balance. Expected ' + expectedBalance + '; Actual ' + actualBalance + '; At address ' + addr);
      });
    }
    
    // Get array of promises that check the balance
    var promises = [];
    for (var key in balances) {
      var p = f([key, balances[key]])
      promises.push(p)
    }

    if (DEBUG_PRINT_RUNNING_BALANCES) console.log('\n' + msg + ' address: balance');
    return Promise.all(promises);
  };

}
