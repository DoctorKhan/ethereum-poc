require('babel-register')

module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: 42
    },
    
    // Ethereum public network
    live: {
      network_id: 1, 
    }
  }
};
