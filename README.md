TODO:
* Create some quantity of redeemable shares
* OasisDex Address: https://etherscan.io/address/0x83ce340889c15a3b4d38cfcd1fc93e5d8497691f#readContract
* Is public redeem of shares algo to expensive? Incorporate map/hash of array indexes for O(1)??

Amit Notes:
* MIT License is used in contracts/ERC20.sol . See file for details
* contracts/FallbackFailer.sol - borrowed code from [repo](https://github.com/makerdao/maker-otc/blob/1f8c7ea0dd7a1d36b7c1efa46b3c20c6f377c75e/contracts/fallback_failer.sol)

# DoGood Blockchain Repo

This blockchain application is designed to create and manage company shares on Ethereum. 

## Design
The following stack is used:
* [MetaMask](https://metamask.io/) - Allows users to run Ethereum dApps in browser without running an Ethereum node.
* [Truffle](http://truffleframework.com/) - Compiles and deploys smart contracts to blockchain, contract testing, etc.
* [Webpack](https://webpack.js.org/concepts/) - Module builder for js apps.
* [React](https://facebook.github.io/react/) - Javascript UI library.
* [Geth](https://github.com/ethereum/go-ethereum/wiki/geth/) - Console that runs Ethereum node. This is used in running the local test net.

File structure
* app/        - web code: .html, .js, .css
* contracts/  - smart contracts: .sol (Solidity)
* test/       - smart contract tests: .sol, .js
* migrations/ - these scripts are run to deploy contracts on the blockchain

## Getting Started

### Installation
The following will be done:
* Install truffle
* Install geth
* Setup local ethereum testnet
* Create an ethereum account

```bash
# Install Geth
sudo apt-get install -y software-properties-common
sudo add-apt-repository -y ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get install -y ethereum

# Install local test net
cd doGood
geth --datadir ~/.ethereum/net42 init genesis42.json

# Enter geth and create new account
geth --datadir ~/.ethereum/net42 --networkid 42 --rpc --rpcport 8545 --rpcaddr 0.0.0.0 --rpccorsdomain "*" --rpcapi "eth,web3,personal,net" console # 1 RPC setup
```
```js
personal.newAccount() // Create new account
personal.listAccounts // View created accounts
exit
```

```bash
# Install Truffle
npm install -g truffle
```

### Deploy - Development
This testnet is used for development. It is free to use and runs local on your machine. 

_ In two different terminals _
#### Terminal 1
When one deploys a contract on the test net, it is necessary to have an Ethereum node running and unlock the miner account. This is described below by the following commands.

Start Geth and run Ethereum node
```bash
geth --datadir ~/.ethereum/net42 --networkid 42 --rpc --rpcport 8545 --rpcaddr 0.0.0.0 --rpccorsdomain "*" --rpcapi "eth,web3,personal,net" console # 1 RPC setup
```
```js
miner.start(1)  // Start mining with one thread
personal.listAccounts(personal.unlockAccount[0]) // This unlocks the mining account to do transactions
```
Stop mining
```js
miner.stop()
```
#### Terminal 2
Deploy contract to local blockchain and run server. Be sure the miner is running.
```shell
cd doGood

# Deploy contract on blockchain
truffle compile --reset 
truffle migrate --reset --network development

# Run server on localhost:8080
npm run build
npm run dev

```

#### Useful Commands
```js
// Send ether to an account
eth.sendTransaction({from:sender, to:receiver, value: amount})

// Check balances of accounts
function checkAllBalances() {
    var totalBal = 0;
    for (var acctNum in eth.accounts) {
        var acct = eth.accounts[acctNum];
        var acctBal = web3.fromWei(eth.getBalance(acct), "ether");
        totalBal += parseFloat(acctBal);
        console.log("  eth.accounts[" + acctNum + "]: \t" + acct + " \tbalance: " + acctBal + " ether");
    }
    console.log("  Total balance: " + totalBal + " ether");
};
```
### Deploy - Production
This is all that is required to deploy the contract on the main network. One must have an Ethereum account with ether in it. When running the geth command, it takes time to sync the blockchain to the computer.

_ In two different terminals: _
#### Terminal 1
```bash
# Deploy to main network blockchain
geth --fast  # Doesn't download entire blockchain database- syncs faster
```
```js
// Unlock account
// Be sure there is ether in the account before deploying
personal.unlockAccount(personal.listAccounts[0]);
```
#### Terminal 2
```bash
# Deploy to main network blockchain
truffle migrate --reset --network live
```

## Notes
* Company name is limited to 32bytes of UTF-8
* Share redeeming: _ Every redeemable share 'code' should be unique. Otherwise, the same person can redeem multiple values. If the redeemable shares exceed the float, then those redeemable shares can't be redeemed. _

## Future
* Stock splitting
 * Concern: If one goes from 100 shares to 1 shares, how is someone with 30 shares affected? Do they lose the stock?
* Voting
* Change float amount
* Token registry: [ERC22](https://github.com/ethereum/EIPs/issues/22)
* DAO
* Mechanisms to allow contracts to be upgraded easily
