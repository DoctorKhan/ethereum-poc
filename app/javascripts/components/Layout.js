import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

// Import libraries we need.
var axios = require('axios');
var sha3 = require('solidity-sha3');
var Web3 = require('web3');
var contract = require('truffle-contract');

// Import our contract artifacts and turn them into usable abstractions.
var events = require('./../events.js')  // This is our code
var dogood_artifacts   = require('../../../build/contracts/DoGood.json');
var company_artifacts  = require('../../../build/contracts/Company.json');
var exchange_artifacts = require('../../../build/contracts/SimpleMarket.json');
var DoGood = contract(dogood_artifacts);
var Company = contract(company_artifacts);
var SimpleMarket = contract(exchange_artifacts);

// var web3;


export default class Layout extends React.Component {


  constructor() {
    super()

    this.state = { account1: '0x0'
                 , account2: '0x0'
                 , account3: '0x0'
                 , companyAddress: '0x0'
                 , gasUsed: 0
                 , receiptNumber: 0
                 , status: 'Started!'
                 , tradeIdOutput: 'N/A'
                 , txResult: 'N/A'
                 };

    this.doGood = undefined;
    this.exchange = undefined;
    this.updateStatus = this.updateStatus.bind(this);


    // Web3 stuff
    if (typeof web3 !== 'undefined') {
      console.warn("Using web3 detected from external source.");
      // Use Mist/MetaMask's provider
      web3 = new Web3(web3.currentProvider);
    } else {
      console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
      // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
      web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
    }

    // Bootstrap the Contract abstractions for Use.
    Company.setProvider(web3.currentProvider);
    DoGood.setProvider(web3.currentProvider);
    SimpleMarket.setProvider(web3.currentProvider);
    DoGood.deployed().then(      (instance) => { this.doGood = instance; });
    SimpleMarket.deployed().then((instance) => { this.exchange = instance; });

    // Load accounts
    web3.eth.getAccounts((err, accs) => {
      if (err != null) {
        this.updateStatus("There was an error fetching your accounts. See logs for details.");
        console.log(err);
      }
      else if (accs.length == 0) 
        this.updateStatus("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
      else {
        this.setState({
          account1: accs[0],
          account2: accs[1],
          account3: accs[2],
        });
      }
    });

  }

  updateStatus = (s) => {
    this.setState({
      status: s,
    });    
  }

  showTxResult = (tx) => {
    var costUsd, ethUsd, s, gasUsed, gasPrice, url, this___;
    this___ = this;
    
    url = 'https://coinmarketcap-nexuist.rhcloud.com/api/eth'
    
    axios.get(url).then((result) => {
      gasUsed = tx['receipt']['gasUsed'];
      ethUsd = Web3.prototype.toBigNumber(result['data']['price']['usd']);

      // Get gas price
      web3.eth.getGasPrice(function(e, gasPrice) {
        costUsd = gasPrice * gasUsed * ethUsd / Web3.prototype.toBigNumber('1000000000000000000');
        costUsd = costUsd.toString(10);
      
        s = 'Gas used- ' + gasUsed + '; '
          + 'Transaction ID- ' + tx['tx'] + '; '
          + 'Price USD- $' + costUsd;
      
        this___.setState({
          txResult: s,
        });    
      });
    });
  }

  
  // This function prints the transaction receipt to console.log
  // See the return object from- https://github.com/ethereum/wiki/wiki/JavaScript-API#web3ethgettransactionreceipt
  printTxReceipt = (e) => {
    e.preventDefault();
    // txHashInput is a 32 byte number. Eg '0xf920..'
    web3.eth.getTransactionReceipt(this.txHashInput.value, function(result) {
      console.log(result);
      this.setState({
        status: result,
      });    
    });
  }

  // This creates a Company.sol contract the company through the DoGood.sol contract 
  registerCompany = (e) => {
    e.preventDefault();

    this.updateStatus('Creating company...');
    this.doGood.registerCompany( this.companyNameInput.value
                               , this.companyAdminInput.value
                               , this.companyTotalSharesInput.value
                               , this.companyPublicSharesInput.value
                               , {from: this.companyAdminInput.value, gas:4000000}
      ).then((result) => {
        console.log(result);
        this.showTxResult(result);
        let companyAddress = events.CompanyCreated(result);
        console.log(companyAddress);
        this.updateStatus("Company created. Address: " + companyAddress);
      }).catch((e) => { 
        this.updateStatus('Error! See console for details!'); 
        console.log(e);
      });
  };

  // This sends company shares to another person/address. The company admin must call this function.
  sendFromCompanyToPerson = (e) => {
    e.preventDefault();

    this.updateStatus('Sending company shares out...');
    this.doGood.companyAddresses(this.companyNameInput.value).then((addr) => {

      return Company.at(addr);
    }).then((company) => {
      
      return company.sendCompanyShares(this.to_sendSharesInput.value, this.value_sendSharesInput.value, {from: this.companyAdminInput.value, gas:4000000})
    }).then((result) => {
      let [from, to, value] = events.Transfer(result);
      this.showTxResult(result);
      
      this.updateStatus('Sent company shares. From ' + from + '; To ' + to + '; Value ' + value);
    }).catch((e) => {
      this.updateStatus('Error! See console for details!');
      console.log(e);
    });
  }

  // Change admin of a company.
  companyChangeCompanyAdmin = (e) => {
    e.preventDefault();

    this.updateStatus('Changing company admin...');
    this.doGood.companyAddresses(this.companyNameInput.value).then((addr) => {

      return Company.at(addr);
    }).then((company) => {

      return company.changeCompanyAdmin(this.company_newAdminInput.value, {from: this.companyAdminInput.value, gas:4000000})
    }).then((result) => {
      this.showTxResult(result);
      this.updateStatus('Changed company admin!');
      console.log(result);
    }).catch((e) => {
      this.updateStatus('Error! See console for details!');
      console.log(e);
    });
  }

  // Create redeemable shares. Meant as incentive program. Has an expire time.
  createRedeemableShares = (e) => {
    e.preventDefault();

    this.updateStatus('Creating redeemable shares...');
    this.doGood.companyAddresses(this.companyNameInput.value).then((addr) => {

      return Company.at(addr);
    }).then((company) => {

      return company.createNewRedeemableShares( this.redeemable_shareCountInput.value
                                              , this.redeemable_expireTimeInput.value
                                              , sha3.default(this.redeemable_secretPhraseInput.value)
                                              , {from: this.companyAdminInput.value, gas:4000000})
    }).then((result) => {
      this.showTxResult(result);
      this.updateStatus('Created redeemable shares.');
      console.log(result);
    }).catch((e) => {
      this.updateStatus('Error! See console for details!');
      console.log(e);
    });
  }

  getCompanyAddress = (e) => {
    e.preventDefault();

    this.updateStatus('Getting company address...');
    this.doGood.companyAddresses(this.companyNameGetBalanceInput.value).then((addr) => {
      this.updateStatus('Got company address.');

      this.setState({
        companyAddress: addr,
        companyName: this.companyNameGetBalanceInput.value,
      });

    }).catch((e) => {
      this.updateStatus('Error! See console for details!');
      console.log(e);
    });
  }


  // Gets a person/addresses balance of company shares
  getBalance = (e) => {
    e.preventDefault();

    this.updateStatus('Getting balance...');
    Company.at(this.state.companyAddress).then((company) => {
      
      return company.balanceOf(this.userAddressGetBalanceInput.value);
    }).then((balance) => { 
      this.updateStatus('Balance of ' + this.userAddressGetBalanceInput.value + ': ' + balance);
      console.log(balance.toString(10));
    }).catch((e) => {
      this.updateStatus('Error! See console for details!');
      console.log(e);
    });
  }

  // User calls this to redeem company shares that were offered
  redeemShares = (e) => {
    e.preventDefault();

    this.updateStatus('Redeeming shares...');
    this.doGood.companyAddresses(this.investor_companyNameInput.value).then((addr) => {

      return Company.at(addr);
    }).then((company) => {

      return company.redeemShares(this.investorAddressInput.value, this.investorRedeem_secretPhraseInput.value, {from: this.investorAddressInput.value, gas: 3000000});
    }).then((result) => {
      this.showTxResult(result);
      this.updateStatus('Redeemed shares!');
      console.log(result);
    }).catch((e) => {
      this.updateStatus('Error! See console for details!');
      console.log(e);
    });
  }

  // A user sends their company shares to another person
  sendShares = (e) => {
    e.preventDefault();

    this.updateStatus('Transfering tokens...');
    this.doGood.companyAddresses(this.investor_companyNameInput.value).then((addr) => {

      return Company.at(addr);
    }).then((company) => {

      // function transfer(address to, uint256 value)
      return company.transfer(this.investor_toAddressInput.value, this.investor_sendValueInput.value, {from: this.investorAddressInput.value});
    }).then((result) => { 
      this.showTxResult(result);
      let [from,to,value] =  events.Transfer(result);
      this.updateStatus('Transfered from ' + from + '; to '+ to + '; value ' + value);
      console.log(result);
    }).catch((e) => {
      this.updateStatus('Error! See console for details!');
      console.log(e);
    });
  }

  // Make a trade offer
  tradeMakeOffer = (e) => {
    e.preventDefault();

    var buyCompanyAddress, sellCompanyAddress;

    this.updateStatus('Approving spending for exchange...');

    this.doGood.companyAddresses(this.buyTokenNameInput.value).then((addr) => {
      buyCompanyAddress = addr;

      return this.doGood.companyAddresses(this.sellTokenNameInput.value);
    }).then((addr) => {
      sellCompanyAddress = addr;

      return Company.at(sellCompanyAddress);
    }).then((sellCompany) => {
      
      // Approve of exchange to take our money escrow style
      return sellCompany.approve(this.exchange.address, this.sellTokenAmountInput.value, {from: this.traderAddressInput.value, gas: 3000000});
    }).then((result) => {
      console.log('NOTE: There are two transactions! One is overwritten in the status, so their is extra gas being used.');
      this.showTxResult(result);
      this.updateStatus('Making offer...');

      return this.exchange.offer(this.sellTokenAmountInput.value, sellCompanyAddress, this.buyTokenAmountInput.value, buyCompanyAddress, {from: this.traderAddressInput.value, gas: 3000000});
    }).then((result) => {
      this.showTxResult(result);
      console.log(result);
      var id = events.ItemUpdate(result);
      if (id == undefined) {
        this.updateStatus('Offer failed!');
        return;
      }
        
      this.setState({tradeIdOutput:id.toString(10),});
      this.updateStatus('Offer sucess! ID- ' + id);
    }).catch((e) => {
      this.updateStatus('Error! See console for details!');
      console.log(e);
    });
  }

  // Display trade orders
  tradeDisplayOrders = (e) => {
    var i;
    
    var fn = (id) => {
      var sellAddress, buyAddress, sellAmount, buyAmount, sellName, buyName, owner;
      return this.exchange.offers(id).then((x) => {
        sellAmount = x[0];
        sellAddress = x[1];
        buyAmount = x[2];
        buyAddress = x[3];
        owner = x[4];
        var active = x[5];
        if (active == undefined || active == false) return {_stop: true};
        
        return Company.at(sellAddress).companyName();
      }).then((name) => {
        if (name['_stop'] != undefined)  return {_stop: true};
        sellName = web3.toAscii(name); // Uses utf-8 actually

        return Company.at(buyAddress).companyName();
      }).then((name) => {
        if (name['_stop'] != undefined)  return {_stop: true};
        buyName = web3.toAscii(name); // Uses utf-8 actually

          console.log( 'Order ID   - ' + id + '\n'
                     + 'Owner      - ' + owner + '\n'
                     + 'Sell Amount- ' + sellAmount + '\n'
                     + 'Sell Name  - ' + sellName + '\n'
                     + 'Buy Amount - ' + buyAmount + '\n'
                     + 'Buy Name   - ' + buyName + '\n'
                     );
      });
    };

    
    this.exchange.last_offer_id().then((last_offer_id) => {
      
      var ids = [];
      for (i = 0; i <= last_offer_id.toNumber(); i++) 
        ids.push(i);

      return Promise.all(ids.map(fn));
    });
  }

  // Look for orders first. Then if found one, call this function to execute.
  tradeBuy = (e) => {
    var sellName, buyName, sellAmount, buyAmount;
    

    this.updateStatus('Approving for exchange...');
    this.doGood.companyAddresses(this.sellTokenNameInput.value).then((addr) => {

      return Company.at(addr);
    }).then((sellCompany) => {
      
      // Approve of exchange to take our money escrow style
      return sellCompany.approve(this.exchange.address, this.tradeBuyAmount2Input.value, {from: this.traderAddressInput.value, gas: 3000000});
    }).then((result) => {
      console.log('NOTE: There are two transactions! One is overwritten in the status, so their is extra gas being used.');
      this.showTxResult(result);
      this.updateStatus('Attempting purchase...');

      return this.exchange.buy( this.tradeIdInput.value
                              , this.tradeBuyAmount2Input.value
                              , {from: this.traderAddressInput.value, gas: 3000000});
    }).then((result) => {
      this.showTxResult(result);
      var  sellAddress, buyAddress;
      [sellAmount, sellAddress, buyAmount, buyAddress] = events.Trade(result);
      var id = events.ItemUpdate(result);

      if (id != this.tradeIdInput.value) {
        this.updateStatus('Buy not sucessful!');
        return;
      }

      this.updateStatus('Buy successful! Traded ' + sellAmount + ' of "' + sellAddress + '" shares for ' + buyAmount + 'of "' + buyAddress + '"'); 
    });
  }

  // Take order off the market. Part of reason is tokens/shares/coins are held in exchange, so user may want those back at some point.
  tradeCancel = (e) => {
    e.preventDefault();

    this.updateStatus('Canceling order...');
    this.exchange.cancel(this.tradeIdInput.value, {from: this.traderAddressInput.value, gas: 3000000}).then((result) => {
      this.showTxResult(result);
      var id = events.ItemUpdate(result);
      if (id)
        this.updateStatus('Order cancelled!');
      else 
        this.updateStatus('Fail. Order not cancelled.');
      console.log(result);
    }).catch((e) => {
      this.updateStatus('Error! See console for details!');
      console.log(e);
    });
  }

  render() {

    return (
<div>
      <p>
        There are three different people using DoGood blockchain. The DoGood admin, the company admin, and the investors. This is reflected in the tabs below. Each tab is indepenedent of the others.
      </p>

        <table>
        <tr>Account 1: {this.state.account1}</tr>
        <tr>Account 2: {this.state.account2}</tr>
        <tr>Account 3: {this.state.account3}</tr>
        <tr>Company '{this.state.companyName}' Address: {this.state.companyAddress}</tr>
        <tr>Status: {this.state.status}</tr>
        <tr>Transaction Result: {this.state.txResult}</tr>
        </table>

        <br/><br/>
        <label>Company Name</label>
        <input id='company-name-getBalance' className='CompanyNameGetBalance' type='text' ref={(i) => { if(i) { this.companyNameGetBalanceInput = i}}} />
        <br/>
        <button className='GetCompanyAddress' 
                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  
                onClick = {this.getCompanyAddress} >Get Company Address</button>
        <br/>
        <label>User Address</label>
        <input id='user-address-getBalance' className='UserAddressGetBalance' type='text' ref={(i) => { if(i) { this.userAddressGetBalanceInput = i}}} />
        <br/>
        <button className='GetBalance' 
                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  
                onClick = {this.getBalance} >Get Balance</button>


        <br/>
        <label>Tx Hash</label>
        <input id='tx-hash' className='TxHash' type='text' ref={(i) => { if(i) { this.txHashInput = i}}} />
        <br/>
        <button className='PrintTxReceipt' 
                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  
                onClick = {this.printTxReceipt} >Print TX Receipt</button>
        
      

        <Tabs>
        <TabList>
        <Tab>DoGood Admin</Tab>
        <Tab>Company Admin</Tab>
        <Tab>Investor</Tab>
        <Tab>Trader</Tab>

        </TabList>

    <TabPanel>

    </TabPanel>

    <TabPanel>
        <label>Company Admin Address</label>
        <input id='company-admin' className='CompanyAdmin' type='text' ref={(i) => { if(i) { this.companyAdminInput = i}}} />
        <br/><br/><br/><br/>
        <label>Company Name</label>
        <input id='company-name' className='CompanyName' type='text' ref={(i) => { if(i) { this.companyNameInput = i}}} />
        <br/>
        <label>Company Total Shares</label>
        <input id='company-totalShares' className='CompanyTotalShares' type='text' ref={(i) => { if(i) { this.companyTotalSharesInput = i}}} />
        <br/>
        <label>Company Public Shares</label>
        <input id='company-publicShares' className='CompanyPublicShares' type='text' ref={(i) => { if(i) { this.companyPublicSharesInput = i}}} />
        <br/>
        <button className='RegisterButton' 
                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  
                onClick = {this.registerCompany} >Register Company</button>

        <br/><br/>
        <label>To Address</label>
        <input id='to-sendShares' className='ToSendShares' type='text' ref={(i) => { if(i) { this.to_sendSharesInput = i}}} />
        <br/>
        <label>Value</label>
        <input id='value-sendShares' className='ValueSendShares' type='text' ref={(i) => { if(i) { this.value_sendSharesInput = i}}} />
        <br/>
        <button className='SendFromCompany' 
                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  
                onClick = {this.sendFromCompanyToPerson} >Send From Company</button>
        <br/>        <br/>
        <label>New Company Admin</label>
        <input id='copmany-newAdmin' className='CompanyNewAdmin' type='text' ref={(i) => { if(i) { this.company_newAdminInput = i}}} />
        <br/>
        <button className='NewAdmin' 
                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  
                onClick = {this.companyChangeCompanyAdmin} >Change Company Admin</button>


        <br/><br/>
        Secrete Phase Example: 0xb10e2d527612073b26eecdfd717e6a320cf44b4afac2b0732d9fcbe2b7fa0cf6
        <br/>
        <label>Secret Phrase</label>
        <input id='redeemable-secretPhrase' className='RedeemableSecretPhrase' type='text' ref={(i) => { if(i) { this.redeemable_secretPhraseInput = i}}} />
        <br/>
        <label>Expire Time</label>
        <input id='redeemable-expireTime' className='RedeemableExpireTime' type='text' ref={(i) => { if(i) { this.redeemable_expireTimeInput = i}}} />
        <br/>
        <label>Reddeemable Share Count</label>
        <input id='redeemable-shareCount' className='RedeemableShareCount' type='text' ref={(i) => { if(i) { this.redeemable_shareCountInput = i}}} />
        <br/>
        <button className='CreateRedeemable' 
                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  
                onClick = {this.createRedeemableShares} >Create Redeemable Shares</button>

    </TabPanel>
    <TabPanel>
        <br/>        <br/>
        <label>Investor Address</label>
        <input id='investor-address' className='InvestorAddress' type='text' ref={(i) => { if(i) { this.investorAddressInput = i}}} />
        <br/>
        <label>Company Name</label>
        <input id='investor-companyName' className='Investor_CompanyName' type='text' ref={(i) => { if(i) { this.investor_companyNameInput = i}}} />
        <br/><br/>
        <label>Secret Phrase</label>
        <input id='investorRedeem-secretPhrase' className='InvestorRedeemSecretPhrase' type='text' ref={(i) => { if(i) { this.investorRedeem_secretPhraseInput = i}}} />
        <br/>
        <button className='RedeemShares' 
                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  
                onClick = {this.redeemShares} >Redeem Shares</button>

        <br/><br/>
        <label>Send Value</label>
        <input id='investor-sendValue' className='InvestorSendValue' type='text' ref={(i) => { if(i) { this.investor_sendValueInput = i}}} />
        <br/>        
        <label>To Address</label>
        <input id='investor-toAddress' className='InvestorToAddress' type='text' ref={(i) => { if(i) { this.investor_toAddressInput = i}}} />
        <br/>        
        <button className='SendShares' 
                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  
                onClick = {this.sendShares} >Send Shares</button>
    </TabPanel>

    <TabPanel>
        <tr>Trade ID: {this.state.tradeIdOutput}</tr>
        <button className='TradeDisplayOrders' 
                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  
                onClick = {this.tradeDisplayOrders} >Display Orders</button>

        <br/><br/>
        <label>Trader Address</label>
        <input id='traderAddress' className='TraderAddress' type='text' ref={(i) => { if(i) { this.traderAddressInput = i}}} />
        <br/>      

        <br/>        
        <label>Buy Token Name</label>
        <input id='buyTokenName' className='BuyTokenName' type='text' ref={(i) => { if(i) { this.buyTokenNameInput = i}}} />
        <br/>        
        <label>Sell Token Name</label>
        <input id='sellTokenName' className='SellTokenName' type='text' ref={(i) => { if(i) { this.sellTokenNameInput = i}}} />
        <br/>        

        <br/>        
        <label>Buy Amount</label>
        <input id='' className='BuyTokenAmount' type='text' ref={(i) => { if(i) { this.buyTokenAmountInput = i}}} />
        <br/>        
        <label>Sell Amount</label>
        <input id='' className='SellTokenAmount' type='text' ref={(i) => { if(i) { this.sellTokenAmountInput = i}}} />
        <br/>        

        <button className='TradeMakerOffer' 
                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  
                onClick = {this.tradeMakeOffer} >Make Offer</button>

        <br/><br/>        
        <label>Trade ID</label>
        <input id='tradeIdInput' className='TradeIdInput' type='text' ref={(i) => { if(i) { this.tradeIdInput = i}}} />
        <br/>        
        <button className='TradeCancelTrade' 
                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  
                onClick = {this.tradeCancel} >Cancel Trade</button>
        <br/>
        <label>Buy Amount 2</label>
        <input id='tradeBuyAmount' className='TradeBuyAmount2' type='text' ref={(i) => { if(i) { this.tradeBuyAmount2Input = i}}} />
        <br/>        
        <button className='TradeBuy' 
                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  
                onClick = {this.tradeBuy} >Buy</button>

    </TabPanel>
        </Tabs>

</div>
    )
  }
}                                                                     
