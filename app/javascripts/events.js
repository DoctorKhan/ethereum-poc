// NOTE: This is used by both test/ and build/ folders

// Events

// ./DoGood.sol
// Returns: [company-address]
var CompanyCreated = function (result) {
  for (var i = 0; i < result.logs.length; i++) {
    var log = result.logs[i];
    if (log.event == 'CompanyCreated') 
      return log['args']['companyAddress'];
  }
  return;
} 

// ./Company.sol
// Returns: [owner, spender, amount/value]
var Approval = function (result) {
  for (var i = 0; i < result.logs.length; i++) {
    var log = result.logs[i];
    if (log.event == 'Approval')
      return [ log['args']['owner']
             , log['args']['spender']
             , log['args']['value']
             ];
  }
  return;
}

// Returns: [from, to, amount/value]
var Transfer = function (result) {
  for (var i = 0; i < result.logs.length; i++) {
    var log = result.logs[i];
    if (log.event == 'Transfer')
      return [ log['args']['from']
             , log['args']['to']
             , log['args']['value']
             ];
  }
  return;
}

// ./SimpleMarket.sol
// Returns: [trade-id]
var ItemUpdate = function (result) {
  for (var i = 0; i < result.logs.length; i++) {
    var log = result.logs[i];
    if (log.event == 'ItemUpdate')
      return log['args']['id'];
  }
  return;
}

// Returns: [sell-amount, sell-address, buy-amount, buy-address]
var Trade = function (result) {
  for (var i = 0; i < result.logs.length; i++) {
    var log = result.logs[i];
    if (log.event == 'Trade')
      return [ log['args']['sell_how_much']
             , log['args']['sell_which_token']
             , log['args']['buy_how_much']
             , log['args']['buy_which_token']];
  }
  return;
}

// ./DoGood.sol
exports.CompanyCreated = CompanyCreated;

// ./Company.sol
exports.Approval = Approval;
exports.Transfer = Transfer;

// ./SimpleMarket.sol
exports.ItemUpdate = ItemUpdate;
exports.Trade = Trade;
