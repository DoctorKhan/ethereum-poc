// https://github.com/makerdao/maker-otc/blob/1f8c7ea0dd7a1d36b7c1efa46b3c20c6f377c75e/contracts/fallback_failer.sol

pragma solidity ^0.4.8;

contract FallbackFailer {
  function () {
    throw;
  }
}
