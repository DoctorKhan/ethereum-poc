pragma solidity ^0.4.8;

import "./ERC20.sol";
import "./SafeMath.sol";
import "./FallbackFailer.sol";

// TODO: One last eye over code. Eg checking modifiers etc.
// TODO: Redeem shares- reserve shares.
contract Company is ERC20, SafeMath, FallbackFailer {

  struct Redeem {
    uint shares;
    bytes32 code;    // hash of secret code must match this: hash(secretCode) == code
    uint expireTime; // Blockchain time
  }

  // ***********************
  // Events

  // ***********************
  // Storage
  mapping (address => uint) private balances;
  mapping (address => mapping (address => uint)) private allowances;  // allowances[owner][spender]
  address private buySellAdmin;           // DoGood uses this when shares are bought w/ bank account
  address public companyAdmin;
  uint public totalSupply;
  uint public publicShares;
  bytes32 public companyName;
  Redeem[] redeemables;

  // ***********************
  // Public functions

  function Company ( bytes32 _companyName
                   , address _companyAdmin
                   , uint _totalSupply
                   , uint _publicShares
                   , address _buySellAdmin)
  isAddressValid(_companyAdmin)
  {
    companyName = _companyName;
    companyAdmin = _companyAdmin;
    totalSupply = _totalSupply;
    publicShares = _publicShares;
    buySellAdmin = _buySellAdmin;
    
    balances[this] = totalSupply;
    throwIf(publicShares > totalSupply);
  }

  // ** ERC20 API

  // Get balance of tokens from 'who' account
  function balanceOf(address who) 
    isAddressValid(who)
    constant returns (uint) 
  {
    return balances[who];
  }
  
  // Transfer tokens
  //  * from: contract caller (msg.sender)
  //  * to:   'to'
  function transfer(address to, uint256 value)
    isAddressValid(to)
    returns (bool success)
  {
    address gainAddress = to;
    address lossAddress = msg.sender;
      
    balances[gainAddress] = safeAdd(balances[gainAddress], value);
    balances[lossAddress] = safeSub(balances[lossAddress], value);

    Transfer(lossAddress, gainAddress, value);
    return true;
  }

  // Transfer tokens. Requires approval- use approve fn.
  // Spender (personA) sends tokens from personB to personC. Requires personB approval.
  //  * from: 'from'
  //  * to:   'to'
  //  * spender: This is contract caller (msg.sender). Spends tokens of 'from' address. 
  function transferFrom(address from, address to, uint value)
    isAddressValid(to)
    isAddressValid(from)
    returns (bool ok)
  {
    address spenderAddress = msg.sender;
    address lossAddress = from;
    address gainAddress = to;

    allowances[lossAddress][spenderAddress] = safeSub(allowances[lossAddress][spenderAddress], value);
    balances[gainAddress] = safeAdd(balances[gainAddress], value);
    balances[lossAddress] = safeSub(balances[lossAddress], value);

    return true;
  }
  
  // The contract caller approves a spender of a value.
  // This sets the value (not additive)
  function approve(address spender, uint value)
    isAddressValid(spender)
    returns (bool ok)
  {
    address owner = msg.sender;

    allowances[owner][spender] = value;
    Approval(owner, spender, value);
    return true;
  }

  // Amount approved by spender of owner account
  function allowance(address owner, address spender)
    isAddressValid(owner)
    isAddressValid(spender)
    constant returns (uint)
  {
    return allowances[owner][spender];
  }

  // ** Public API

  // Redeem shares. redeemCode is secret code.
  function redeemShares(address receiver, bytes32 secretCode) {
    uint i;
    uint shares;
    uint len = redeemables.length;
    Redeem memory redeem;
    bytes32 redeemCode = sha3(secretCode);

    for (i = 0; i < len; i++) {
      redeem = redeemables[i];

      // If found matching code
      if (redeem.code != redeemCode) continue;

      // Delete and resize
      delete redeemables[i];  // Use this in various cases, such as i = 0
      redeemables[i] = redeemables[len - 1];
      redeemables.length = len - 1;

      // Send shares if not expired
      if (block.timestamp < redeem.expireTime)
        sendCompanyShares_private(receiver, redeem.shares);
      
      break;
    }
  }

  // ** Company/DoGood admin API

  // Create redeemable shares. Has expire time too
  // Consider how many shares giving out. Note, it is impossible to issue more shares than the public-shares/float.
  function createNewRedeemableShares(uint value, uint expireTimeSecondsInFuture, bytes32 unSecretCode)
    throwIfM(value == 0)
    isCompanyAdmin(msg.sender)
  {
    uint expireTime = safeAdd(block.timestamp, expireTimeSecondsInFuture);
    Redeem memory redeem = Redeem(value, unSecretCode, expireTime);

    redeemables.push(redeem);
  }

  // Send company shares to someone. The admin does this on behalf of the company, by being the caller.
  // Must reach goal before time runs out
  function sendCompanyShares(address to, uint  value)
    assertM(  msg.sender == buySellAdmin
           || msg.sender == companyAdmin)
    isAddressValid(to)
  {
    sendCompanyShares_private(to, value);
  }

  function sendCompanyShares_private(address to, uint value)
  {
    bool haveNotExceededPublicSupply;

    balances[this] = safeSub(balances[this], value);
    balances[to] = safeAdd(balances[to], value);

    haveNotExceededPublicSupply = balances[this] >= safeSub(totalSupply, publicShares);

    assert(haveNotExceededPublicSupply);
    Transfer(this, to, value);
  }

  // This deletes the company. Used more by calling the parent contract which calls this (eg doGood)
  function deleteContract()
    isBuySellAdmin(msg.sender)
  {
    selfdestruct(msg.sender);
  }

  // Change admin of company. Requires admin to do this
  function changeCompanyAdmin(address newAdmin)
    isCompanyAdmin(msg.sender)
    isAddressValid(newAdmin)
  {
    companyAdmin = newAdmin;
  }

  function changeBuySellAdmin(address newAdmin)
    isBuySellAdmin(msg.sender)
    isAddressValid(newAdmin)
  {
    companyAdmin = newAdmin;
  }

  // Anyone can run this
  // This is meant to remove unused data, like garbage collection.
  // Eg Delete unredeemed shares overtime limit 
  function cleanUp() {
    uint i;
    uint i2;
    Redeem memory redeem;
    uint currentTime = block.timestamp;
    uint len;

    // ** Redeemable shares

    len = redeemables.length;
    // First pass is deleting elements 
    for (i = 0; i < len; i++) {
      if (redeemables[i].expireTime == currentTime)
        delete redeemables[i];
    }
    // Second pass is filling in deleted elements
    i2 = len;
    for (i = 0; i < len; i++) {
      if (redeemables[i].shares != 0) continue;

      i2 = getNextValidRedeemablesFromBack(i2);
      if (i >= i2) break;
      redeemables[i] = redeemables[i2];
    }
    redeemables.length = i;

    // ** ..
  }  

  // *************************
  // Private functions

  function getNextValidRedeemablesFromBack(uint i) private returns (uint) {
    while(i != 0) {
      i = i - 1;
      if (redeemables[i].shares != 0)
        break;
    }
    return i;
  }

  // *************************
  // Modifiers
  modifier isAddressValid (address addr) {
    throwIf(addr == 0);  // Check if null. I don't remember why, but was taught in B9Lab certifaction/course
    _;
  }

  modifier isBuySellAdmin (address addr) {
    assert(addr == buySellAdmin);
    _;
  }

  modifier isCompanyAdmin (address addr) {
    assert(addr == companyAdmin);
    _;
  }  

  modifier assertM(bool condition) {
    assert(condition);
    _;
  }

  modifier throwIfM(bool condition) {
    throwIf(condition);
    _;
  }


}
