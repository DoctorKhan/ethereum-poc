pragma solidity ^0.4.8;

import "./Company.sol";
import "./SafeMath.sol";
import "./FallbackFailer.sol";

contract DoGood is Utils, SafeMath, FallbackFailer {
  mapping (bytes32 => address) public companyAddresses;
  address private admin;

  // ****************************
  // Events
  event CompanyCreated(address indexed companyAddress);

  // ****************************
  // Public functions
  function DoGood (address doGoodAdmin) 
    isAddressValid(doGoodAdmin)
  {
    admin = doGoodAdmin;
  }

  // ** Public API
  function registerCompany( bytes32 companyName
                          , address companyAdmin
                          , uint totalSharesOutstanding
                          , uint totalFloat)
    isNonDuplicateCompanyName(companyName)
    returns (address)
  {
    address company
      = new Company( companyName
                   , companyAdmin
                   , totalSharesOutstanding
                   , totalFloat
                   , admin);          // Default is DoGood admin

    companyAddresses[companyName] = company;
    CompanyCreated(company);

    return company;
  }

  // ** DoGood admin API


  function deleteCompany(bytes32 companyName)
    isAdminAddress(msg.sender)
  {
    Company(companyAddresses[companyName]).deleteContract();
    delete companyAddresses[companyName];
  }

  // Change admin of company. Requires admin to do this
  function changeAdmin(address newAdmin)
    isAdminAddress(msg.sender)
    isAddressValid(newAdmin)
  {
    admin = newAdmin;
  }

  // ****************************
  // Modifiers
  modifier isAddressValid (address addr) {
    throwIf(addr == 0);  // Check if null. I don't remember why, but was taught in B9Lab certifaction/course
    _;
  }

  modifier isAdminAddress (address addr) {
    assert(addr == admin);
    _;
  }

  modifier isNonDuplicateCompanyName (bytes32 name) {
    assert(companyAddresses[name] == 0);
    _;
  }
}
